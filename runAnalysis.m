% Calling script for processing of multi-TE data series
% Note: Code comments are supplementary to the readme file
%
% (c) Institute for Biomedical Engineering, ETH and University of Zurich, Switzerland

addpath(genpath(pwd)); % add subfolders of current folder to path

%% User options

pars.sample      = 'D2O';  % Which sample to analyse: 'D2O' (1Ds in original paper), 'H2O' (1Hs in original paper), or 'MS' (A1 in multiple sclerosis paper)
                      
% Type of fit to run
% 'open': All parameters of the signal model are free (except W-component T2 and CS)
% 'fixed': U- and S-component T2 and CS are also fixed
pars.fitType     = 'open'; 

% Type of analysis to perform
% 'map': Single-voxel fits to generate map of all free model parameters 
%            Recommended use: fitType 'fixed'
%            Parallel-enabled: Start parallel pool manually
% 'comp': Component analysis of single voxels or averaged signals (specified in pars.roi)
%            Recommended use: 
%                fitType 'open' to investigate T2s and CSs
%                fitType 'fixed' to investigate As
analysis         = 'comp';  

% Signal to analyse (for analysis 'comp')
% Option 1: Single voxel, give coordinate array [X Y Z]
% Option 2: Multiple voxels (signal across voxels will be averaged)
%               Provide list of coordinates ([X1 Y1 Z1; X2 Y2 Z2; ...])
%               Use predefined voxel lists 
%                    'D2O' or 'H2O' samples: 'WM', 'GM'
%                    'MS' sample: 'WM_left', 'WM_right', 'GM_lower', 'GM_upper', 'MS_left', 'MS_right'
pars.roi         = 'WM';    

% Off-resonance correction before fitting (0 = no, 1 = yes)
% Only valid for H2O sample (but no restriction is built into the code)
% Recommended use: 
%     1 for analysis 'comp' where roi contains multiple voxels
%     0 for analysis 'map' (the correction here is redundant unless the local off-resonance is outside the range of the 'df' fit parameter (see myelinFit function))
pars.offResCorr  = 0;    

% Slices (in X) to apply map fitting to ([] = all) 
% Note: Multiple slices are time-consuming, we recommend 
%           'D2O' sample: Slices 36, 37 and/or 38 (paper slice: 37)
%           'H2O' sample: Slices 35, 36, and/or 37 (paper slice: 37)
%           'MS' sample: Slices 36 to 41 (paper slice: 36)
pars.xSlices     = 37;   

%% Load data

load(['data_' pars.sample], 'data'); % loads data (from current folder) 

%% Parameters (can be edited by advanced users)

% Indices of TE values to use ([] = all)
% The D2O sample has 14 TEs, and the H2O sample has 12 TEs 
pars.indTe       = []; 

% Number of starting points for fit (see Matlab documentation for MultiStart)
pars.nStart = 10;     

% Number of super-Lorentzian components in signal model
% An option is included for nS = 1 (see fitType below and computeValComp
% function), any other options are outside the intended use of this code 
% and must be implemented by the interested user
pars.nS = 2;

switch pars.fitType
    case 'open'
        pars.nameFit     = {'ph' 'df' 'aL' 'aS1' 'aS2' 't2S1' 't2S2' 'dfS1' 'dfS2'}; % name of parameters to fit
        pars.nameConst   = {'typeS1' 'typeS2' 't2L'}; % name of parameters to fix
        pars.parConst    = [1 1 50]; % value of parameters to fix (same order as nameConst)
    case 'fixed'
        % Note: The last 4 entries of parConst can be adapted to try different components (e.g. GM averages from paper). 
        % S1 should have the shortest T2, and df is given in kHz (hence cs2df conversion)
        pars.nameFit     = {'ph' 'df' 'aL' 'aS1' 'aS2'};
        pars.nameConst   = {'typeS1' 'typeS2' 't2L' 't2S1' 't2S2' 'dfS1' 'dfS2'};
        pars.parConst    = [1 1 50 5.48e-3 102e-3 cs2df(1.07,data.f0) cs2df(2.09,data.f0)];  
    case 'nS1_open'
        pars.nameFit     = {'ph' 'df' 'aL' 'aS1' 't2S1' 'dfS1'};
        pars.nameConst   = {'typeS1' 't2L'};
        pars.parConst    = [1 50];
end

% Start values and boundaries (if wish to overwrite the default given in myelinFit)
% This option is included for interested users
%     Example:
%     pars.sb.name = {'t2S1' 't2S2'};
%     pars.sb.sv   = [5e-3   100e-3]; % start value
%     pars.sb.lb   = [1e-3   50e-3 ]; % lower boundary
%     pars.sb.ub   = [20e-3  200e-3]; % upper boundary
pars.sb = [];

%% Call functions

switch analysis
    case 'comp'
        fitComp(pars, data);
    case 'map'
        fitMap(pars, data);
end
