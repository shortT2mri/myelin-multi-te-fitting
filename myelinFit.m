function [parFit, res, se, rmse, hasSol] = myelinFit(s, t, nameFit, nS, parConst, nameConst, nStart, noiseStd, sb, multipleSol)
% Fit complex time-domain signal of myelin + H2O
%
% IN
%   s            Time-domain signal
%   t            Time points [time_units]
%   nameFit      Names of fitted parameters
%   nS           Number of short-T2 components 
%   parConst     Values of constant parameters
%   nameConst    Names of constant parameters
%   nStart       Number of starting points
%   noiseStd     Standard deviation of noise on 
%                Scalar or vector with value for each time point
%                [] = get from residuals
%   sb           Structure with start values and boundaries to overwrite (can be empty)
%                Example:
%                sb.name = {'t2S1' 't2S2'};
%                sb.sv   = [5e-3   100e-3];
%                sb.lb   = [Inf    50e-3 ];
%                sb.ub   = [20e-3  Inf   ];
%   multipleSol  Return all viable solutions (1) or just the optimal solution (0) {0}
%
% OUT (one row per viable solution)
%   parFit       Values of fitted parameters
%   res          Squared 2-norm of the residual
%   se           Standard error per parameter
%   rmse         Root-mean-square error of fit
%   hasSol       Returns 1 if fit solution was found, 0 if no solution found 
%
% (c) Institute for Biomedical Engineering, ETH and University of Zurich, Switzerland

%% Initialisation

if nargin < 10, multipleSol = 0; end

Ftol = 1e-5;
Xtol = 1e-2;
rmseNorm = 1; % Type of normalisation of rmse: 0 = none, 1 = mean, 2 = min-max

parFit = zeros(1,length(nameFit));
res = 0;
se = zeros(1,length(nameFit));
rmse = zeros(1,length(nameFit));
hasSol = 1;

%% Start values and boundaries

% Set values for all parameters
name = {'ph'  'df' 'aL' 't2L'};
sv   = [0     0    1    10   ]; % start value
lb   = [0    -0.5  0    1    ]; % lower boundary
ub   = [2*pi  0.5  20   100  ]; % upper boundary
for iS = 1:nS
    ind = num2str(iS);
    name = [name ['typeS' ind] ['dfS' ind] ['aS' ind] ['t2S' ind]];
    sv = [sv 1  0.5 1   1e-2];
    lb = [lb 0  0   0   1e-3];
    ub = [ub 1  1   20  1   ];
end

% Overwrite with user settings
if ~isempty(sb)
    for iSb = 1:length(sb.name)
        iPar = ismember(name, sb.name{iSb});
        if ~isinf(sb.sv(iSb))
            sv(iPar) = sb.sv(iSb);
        end
        if ~isinf(sb.lb(iSb))
            lb(iPar) = sb.lb(iSb);
        end
        if ~isinf(sb.ub(iSb))
            ub(iPar) = sb.ub(iSb);
        end
    end
end

% Prepare for fit
nFit = length(nameFit);
svFit = zeros(1, nFit);
lbFit = zeros(1, nFit);
ubFit = zeros(1, nFit);
for iFit = 1:nFit
    [~, ind] = ismember(nameFit{iFit}, name);
    if ind > 0
        svFit(iFit) = sv(ind);
        lbFit(iFit) = lb(ind);
        ubFit(iFit) = ub(ind);
    else
        error('Parameter %s not found in list', nameFit{iFit});
    end
end

%% Prepare signal

% Normalise
maxS = max(abs(s));
s = s / maxS;
noiseStd = noiseStd / maxS;

% Make real
s = [real(s) imag(s)];

%% Perform fit

% Create optimisation problem
fun = @(parFit, t)myelinSignal(parFit, t, nameFit, nS, parConst, nameConst, 1);
options = optimset('Display', 'off');
problem = createOptimProblem('lsqcurvefit', 'objective', fun, 'x0', svFit, 'lb', lbFit, 'ub', ubFit, 'xdata', t, 'ydata', s, 'options', options);

% Set up and run multi-start fit
rng default % random number reproducibility
ms = MultiStart('Display', 'off', 'FunctionTolerance', Ftol, 'XTolerance', Xtol, 'UseParallel', true, 'StartPointsToRun', 'bounds');
[~, ~, exitflag, ~, gos] = run(ms, problem, nStart);

if ~ismember(exitflag, [1 2])
    hasSol = 0;
    return
end

%% Evaluate all solutions that are in the same optimal Fval region (within Ftol)

Fvals = [gos(:).Fval];
solIdx = find(abs(Fvals-Fvals(1)) < Ftol);

if ~isempty(noiseStd)
    if length(noiseStd) == 1
        noiseStd = ones(1, length(s)/2) * noiseStd;
    end
    noiseStd = [noiseStd(:); noiseStd(:)];
    invNoiseCov = diag(1 ./ noiseStd.^2);
end

nSol = length(solIdx);
if multipleSol == 0, nSol = 1; end

for iSol = 1:nSol
    
    % Single-start fit from the starting points determined in MultiStart fit 
    % (Identical fit, but need to call lsqcurvefit() directly to access error properties)
    problem.x0 = gos(solIdx(iSol)).X0{1};
    [parFit(iSol,:), res(iSol,:), residual(iSol,:), ~, ~, ~, Jsp] = lsqcurvefit(problem);
    J(:,:,iSol) = full(Jsp); 

    %% Calculate errors

    % Error on parameters
    if isempty(noiseStd)
        [~, se(iSol,:)] = nlparcise(parFit(iSol,:), residual(iSol,:), 'jacobian', sparse(J(:,:,iSol)));  
    else
        parCov = inv(J(:,:,iSol)' * invNoiseCov * J(:,:,iSol));
        se(iSol,:) = sqrt(diag(parCov));
    end

    % Fit error
    rmse = sqrt(res(iSol,:) / length(s));
    switch rmseNorm
        case 1
            rmse(iSol,:) = rmse / mean(s);
        case 2
            rmse(iSol,:) = rmse / (max(s)-min(s));
    end


    %% Undo normalisation

    for iPar = 1:length(parFit(iSol,:))
        if nameFit{iPar}(1) == 'a'
            parFit(iSol,iPar) = parFit(iSol,iPar) * maxS;
            se(iSol,iPar) = se(iSol,iPar) * maxS;
        end
    end
    
end

%#ok<*AGROW>
