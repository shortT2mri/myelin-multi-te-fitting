function fitComp(pars, data)
% Fit average of selected points in multi-TE data series
%
% IN
%   pars     Structure containing analysis parameters (see runAnalysis)
%   data     Structure containing data
%
% (c) Institute for Biomedical Engineering, ETH and University of Zurich, Switzerland

%% Get list of voxels

if ischar(pars.roi) % predefined voxel list
    switch pars.roi
        case 'WM'
            pos = data.pWM;
        case 'GM'
            pos = data.pGM;
        case 'WM_left'
            pos = data.pWM_left;
        case 'WM_right'
            pos = data.pWM_right;
        case 'GM_lower'
            pos = data.pGM_lower;
        case 'GM_upper'
            pos = data.pGM_upper;
        case 'MS_left'
            pos = data.pMS_left;
        case 'MS_right'
            pos = data.pMS_right;

    end
else % user-defined
    pos = pars.roi;
end

%% Get data and parameters

d = data.d;
te = data.te;
noiseStd = data.noiseStd;
nTe = data.nTe;
if ~isempty(pars.indTe) % Use select TEs 
    te = te(pars.indTe);
    nTe = length(pars.indTe);
    d = d(:,:,:,pars.indTe);
    if ~isempty(noiseStd)
        noiseStd = noiseStd(pars.indTe);
    end
end

%% Get signal values

nP = size(pos, 1);
val = zeros(nP, nTe);
for iP = 1:nP % go through all voxels in list
    val(iP, :) = squeeze(d(pos(iP, 1), pos(iP, 2), pos(iP, 3), :)).';
    
    % Local off-resonance correction
    if pars.offResCorr == 1
        val(iP, :) = performOffResCorr(te, val(iP, :));
    end

end

% Average all voxels
val = mean(val, 1);

%% Perform fit

% Parameters entitled 'Full' contain all viable solutions (see myelinFit.m) 
% found by the fit, which can be examined by the interested user
[parFitFull, resFull, seFull, nrmseFull, hasSol] = myelinFit(val, te, pars.nameFit, pars.nS, pars.parConst, pars.nameConst, pars.nStart, noiseStd / sqrt(nP), pars.sb, 1);

if ~hasSol; error('No solution found'); end

%% Return first (optimal) solution    

parFit = parFitFull(1,:);
res = resFull(1,:);
se = seFull(1,:);
nrmse = nrmseFull(1,:);

% Get values for fit
nFit = length(pars.nameFit);
name = [pars.nameConst pars.nameFit];
par = [pars.parConst parFit];
teFit = [0 logspace(log10(1e-3), log10(max(te)), 50)];

valFit = myelinSignal(par, teFit, name, pars.nS);

%% Sort s.t. SL-components have T2s in ascending order (S1 has shortest T2 etc.)

if strcmp(pars.fitType, 'open')
    t2 = zeros(1, pars.nS);
    t2Idx = zeros(1, pars.nS);
    amIdx = zeros(1, pars.nS);
    dfIdx = zeros(1, pars.nS);
    for iS = 1:pars.nS
        t2(iS) = getPar(['t2S' num2str(iS)], parFit, pars.nameFit);
        t2Idx(iS) = getPar(['t2S' num2str(iS)], 1:nFit, pars.nameFit);
        amIdx(iS) = getPar(['aS' num2str(iS)], 1:nFit, pars.nameFit);
        dfIdx(iS) = getPar(['dfS' num2str(iS)], 1:nFit, pars.nameFit);
    end
    [~, sortIdx] = sort(t2, 'ascend');

    parIdx = 1:nFit;
    parIdx(t2Idx) = parIdx(t2Idx(sortIdx));
    parIdx(amIdx) = parIdx(amIdx(sortIdx));
    parIdx(dfIdx) = parIdx(dfIdx(sortIdx));

    parFit = parFit(parIdx);
    se = se(parIdx);
end

%% Display

if ischar(pars.roi)
    roitxt = pars.roi;
else
    roitxt = 'custom_roi';
end
titl = [mfilename '_' roitxt];

disp('--------------------')
disp('Displaying fit results')
disp(['Residual: ' num2str(res,10)])

% Output text
displayFit(pars, parFit, se, data);

% Figure
valComp = computeValComp(parFit, pars.nameFit, pars.nameConst, pars.parConst, teFit, pars.fitType);
fitPlot(te, val, teFit, valFit, valComp, titl, [par res nrmse], [name 'res' 'nrmse']);



