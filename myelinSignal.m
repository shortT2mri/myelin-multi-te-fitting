function s = myelinSignal(parFit, t, nameFit, nS, parConst, nameConst, sReal)
% Complex time-domain signal of myelin + H2O to be used for fitting
%
% The model provides
%   - One long-T2 component with exponential decay (H2O)
%   - Multiple additional (short-T2) components
%
% PARAMETERS   Describing the model
%   Names      -> nameFit, nameConst
%   Values     -> parFit, parConst
%
%   typeSx     Type of short-T2 signal
%              0 = Lorentzian
%              1 = Super-Lorentzian
%              Note: not suited for fitting as discrete
%   ph         Global phase [rad]
%   df         Off-resonance of long-T2 component [kHz]
%   aL         Integral amplitude of long-T2 component
%   t2L        T2 of (exponential) long-T2 component [ms]
%   dfSx       Relative off-resonance of short-T2 component x [kHz]
%   aSx        Integral amplitude of short-T2 component x
%   t2Sx       T2/T2min of short-T2 component x [ms]
%
% IN
%   parFit     Values of fitted parameters
%   t          Time points [ms]
%   nameFit    Names of fitted parameters (cell array)
%   nS         Number of short-T2 components {1}
%   parConst   Values of constant parameters {empty}
%   nameConst  Names of constant parameters (cell array) {empty}
%   sReal      1 = make signal real by splitting real and imaginary part {0}
%
% OUT
%   s          Time-domain signal
%
% (c) Institute for Biomedical Engineering, ETH and University of Zurich, Switzerland


%% Defaults

if nargin < 4, nS = 1; end
if nargin < 5, parConst = []; end
if nargin < 6, nameConst = []; end
if nargin < 7, sReal = 0; end

N = 150; % Number of Lorentzians per super-Lorentzian


%% Transfer parameters

name  = [nameFit nameConst];
par   = [parFit parConst];
ph    = getPar('ph',  par, name);
df    = getPar('df',  par, name);
aL    = getPar('aL',  par, name);
t2L   = getPar('t2L', par, name);
typeS = zeros(1, nS);
dfS   = zeros(1, nS);
aS    = zeros(1, nS);
t2S   = zeros(1, nS);
for iS = 1:nS
    typeS(iS) = getPar(['typeS' num2str(iS)],  par, name);
    dfS(iS)   = getPar(['dfS' num2str(iS)],  par, name);
    aS(iS)    = getPar(['aS'  num2str(iS)],  par, name);
    t2S(iS)   = getPar(['t2S' num2str(iS)],  par, name);
end


%% Calculate signal

% Long-T2
if aL == 0 % otherwise t=0 gives NaN
    sL = 0;
else
    sL = aL * exp(-t / t2L);
end

% Short-T2
sS = zeros(nS, length(t));
for iS = 1:nS
    if typeS(iS) == 0
        sS(iS, :) = exp(-t / t2S(iS));
    else
        sS(iS, :) = superLorentzian(t, 1 / (2*pi * t2S(iS)), 0, 0, N, 1);
    end
    sS(iS, :) = aS(iS) * sS(iS, :) .* exp(-1i * 2*pi * dfS(iS) * t);
end

% Combine
s = exp(1i * (ph + 2*pi * df * t)) .* (sL + sum(sS, 1));


%% Make real

if sReal
    s = [real(s) imag(s)];
end

