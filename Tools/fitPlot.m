function fitPlot(te, val, teFit, valFit, valComp, titl, par, name, norm)
% Plot signal of multi-TE SPI data series
%
% IN
%   te        Echo times of measured data [ms]
%   val       Complex-valued measured data
%   teFit     Echo times of fitted data [ms] {[]}
%   valFit    Complex-valued fitted data {[]}
%   titl      Figure title base {mfilename}
%   par       Values of parameters shown in title {[]}
%   name      Names of parameters shown in title {''}
%   valComp   Structure containing complex valFit parameters of components
%   norm      1 = normalise magnitude by value of fit at time zero {0}
%             If fit does not exists, normalise by datapoints
%
% (c) Institute for Biomedical Engineering, ETH and University of Zurich, Switzerland


%% Defaults

if nargin < 3, teFit = []; end
if nargin < 4, valFit = []; end
if nargin < 5, valComp = []; end
if nargin < 6 || isempty(titl), titl = mfilename; end
if nargin < 7, par = []; end
if nargin < 8, name = ''; end
if nargin < 9, norm = 0; end


%% Prepare

% Create title
if ~isempty(par) && ~isempty(name)
    nPar = length(par);
    for iPar = 1:nPar
        titl = sprintf('%s %s = %.2g', titl, name{iPar}, par(iPar));
        if iPar < nPar
            titl = sprintf('%s, ', titl);
        end
    end
end

% Normalise magnitude
if norm == 1 
    if ~isempty(valFit)
        maxVal = max(abs(valFit));
    else
        maxVal = max(abs(val));
    end
else
    maxVal = 1;
end


%% Create figure

figure('Units', 'normalized', 'OuterPosition', [0.2 0.2 0.5 0.55], 'Name', titl); 

% Magnitude
subplot(1, 2, 1);
plot(te, abs(val)/maxVal, 'ko', 'DisplayName', 'Data', 'LineWidth', 2);
xlabel('TE [ms]');
if norm == 1; ylabel('Magnitude [normalised]'); else; ylabel('Magnitude'); end
if ~isempty(valFit)
    hold on;
    plot(teFit, abs(valFit)/maxVal, 'k', 'DisplayName', 'Fit', 'LineWidth', 2);
    hold off;
end
if ~isempty(valComp)
    fields = fieldnames(valComp);
    hold on;
    for idxComp = 1:numel(fields)
        h = plot(teFit, abs(extractfield(valComp, fields{idxComp}))/maxVal, '--', 'DisplayName', fields{idxComp}, 'LineWidth', 2);
        if fields{idxComp} == 'U'; h.Color = '#A2142F'; elseif fields{idxComp} == 'S'; h.Color = '#EDB120'; ...
        elseif fields{idxComp} == 'W'; h.Color = '#0072BD'; else; h.Color = '#77AC30'; end 
    end
    hold off;   
end
xlim([0 1.02*max(te)]);
ax = gca; ax.FontSize = 14; ax.LineWidth = 1; ax.Color = [0.97 0.97 0.97];

% Phase
ax2 = subplot(1, 2, 2);
ax2.LineWidth = 1;
ax2.Color = [0.97 0.97 0.97];
plot(te, angle(val), 'ko', 'DisplayName', 'data', 'LineWidth', 2);
xlabel('TE [ms]');
ylabel('Phase [rad]');
if ~isempty(valFit)
    hold on;
    plot(teFit, unwrap(angle(valFit)), 'k', 'DisplayName', 'fit', 'LineWidth', 2);
    hold off;
end
if ~isempty(valComp)
    fields = fieldnames(valComp);
    hold on;
    for idxComp = 1:numel(fields)
        h = plot(teFit, unwrap(angle(extractfield(valComp, fields{idxComp}))), '--', 'DisplayName', fields{idxComp}, 'LineWidth', 2);
        if fields{idxComp} == 'U'; h.Color = '#A2142F'; elseif fields{idxComp} == 'S'; h.Color = '#EDB120'; ...
        elseif fields{idxComp} == 'W'; h.Color = '#0072BD'; else; h.Color = '#77AC30'; end 
    end
    hold off;
end
xlim([0 1.02*max(te)]);
legend('Box','off', 'Location', 'southwest');
ax = gca; ax.FontSize = 14; ax.LineWidth = 1; ax.Color = [0.97 0.97 0.97];

set(gcf,'color','w');
