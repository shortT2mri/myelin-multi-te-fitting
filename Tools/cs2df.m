function df = cs2df(cs, f0, noShift)
% Converts between relative (cs, ppm) and absolute (df, kHz) chemical shifts
% noShift parameter governs shift w.r.t. water (4.7 ppm)
%
% (c) Institute for Biomedical Engineering, ETH and University of Zurich, Switzerland

if ~exist('noShift', 'var')
    cs = 4.7 - cs;
end

df = cs * f0 * 1e-6;

