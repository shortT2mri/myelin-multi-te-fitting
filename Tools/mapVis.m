function mapVis(maps, nameMaps, pars, inter)
% Display parameter maps from multi-TE fitting
%
% Note that this is basic visualisation code. It is not intended for close 
% examination of the parameter maps, but can act as a basis for further 
% development by the interested user
%
% The images appear rather small if >3 xSlices are used
%
% IN
%   maps        4D array containing the 3D image for each map (4th dimension)
%   nameMaps    Cell array of names for each parameter map
%   pars        Structure containing analysis parameters (see runAnalysis)
%   inter       Interpolation factor to be applied for display {1}
%
% (c) Institute for Biomedical Engineering, ETH and University of Zurich, Switzerland

if nargin < 4; inter = 1; end

nMaps = size(maps,4);
nSlices = length(pars.xSlices);

figure('Units', 'normalized', 'OuterPosition', [0.1 0.1 0.8 0.8], 'Name', [mfilename ' *Note that images are cropped and interpolated*']); 
for iSlice = 1:nSlices
    for iMap = 1:nMaps
        subplot(nSlices, nMaps, iMap+((iSlice-1)*nMaps))
        imgslice = squeeze(maps(pars.xSlices(iSlice),:,:,iMap));
        
        % crop image [xmin ymin width height] (values chosen to fit the example data)
        switch pars.sample
            case {'D2O', 'H2O'}
                imgslice = imcrop(imgslice, [12 8 41 41]);
            case 'MS'
                imgslice = imcrop(imgslice, [12 6 41 41]);
        end
        
        % interpolate image
        imgslice = imresize(imgslice, size(imgslice) * inter);
        
        imshow(imgslice, []); set(gca, 'ydir', 'normal'); 
        colorbar('location', 'northoutside')
        if iSlice == nSlices
            xlabel(nameMaps{iMap})
        end
        if iMap == 1
            ylabel(['xSlice ' num2str(pars.xSlices(iSlice))])
        end
        
    end
end

set(gcf,'color','w');
