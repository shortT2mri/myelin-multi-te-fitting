function cs = df2cs(df, f0, noShift)
% Converts between absolute (df, kHz) and relative (cs, ppm) chemical shifts
% noShift parameter governs shift w.r.t. water (4.7 ppm)
%
% (c) Institute for Biomedical Engineering, ETH and University of Zurich, Switzerland

cs = df / f0 * 1e6 ; % [ppm]

if ~exist('noShift', 'var')
    cs = 4.7 - cs;
end

