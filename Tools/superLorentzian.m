function [sl, theta, c, scal] = superLorentzian(x, lambda, centre, norm, N, ft, df, weight)
% Calculate super-Lorentzian function
%
% IN
%   x         Argument
%   lambda    Full width at half maximum {1}
%   centre    Position of maximum {0}
%   norm      Normalisation (0 = integral, 1 = maximum) {0}
%   N         Number of Lorentzians {100}
%   ft        Calculate corresponding super-exponential instead {0}
%   df        Additional line broadening by B0 non-uniformity {0}
%   weight    Weight per Lorentzian {1}
%
% OUT
%   sl        Function values
%   theta     Orientation angles
%   c         Broadening constant per angle
%   scal      Scaling factor per angle
%
% REFERENCE
%   M.J. Wilhelm, Proc Natl Acad Sci, 109 (2012) 9605
%
% (c) Institute for Biomedical Engineering, ETH and University of Zurich, Switzerland

%% Defaults

default_lambda  = 1;
default_centre  = 0;
default_norm    = 0;
default_N       = 1e2;
default_ft      = 0;
default_df      = 0;
default_weight  = 1;

npar = 1;
if nargin < npar
    error('At least %d input parameter(s) required', npar);
end
npar = npar + 1; if (nargin < npar) || isempty(lambda), lambda = default_lambda; end
npar = npar + 1; if (nargin < npar) || isempty(centre), centre = default_centre; end
npar = npar + 1; if (nargin < npar) || isempty(norm), norm = default_norm; end
npar = npar + 1; if (nargin < npar) || isempty(N), N = default_N; end
npar = npar + 1; if (nargin < npar) || isempty(ft), ft = default_ft; end
npar = npar + 1; if (nargin < npar) || isempty(df), df = default_df; end
npar = npar + 1; if (nargin < npar) || isempty(weight), weight = default_weight; end


%% Calculate

theta = linspace(0, pi/2, N);
[Theta, X] = ndgrid(theta, x);
c = abs(3 * cos(Theta).^2 - 1) + eps; % eps: avoid division by 0 in lorentz
scal = sin(Theta);

sl = sum(scal .* repmat(weight.', 1, length(x)) .* lorentz(X, lambda * c + df, centre, 0, ft), 1);


%% Normalise

if norm == 0
    sl = sl / sum(scal(:, 1));
else
    sl = sl / max(sl(:)); % ft = 1: not corresponding to normalisation of SL lineshapes
end


%% Output

if size(sl, 1) ~= size(x, 1)
    sl = sl.';
end

c = c(:, 1);
scal = scal(:, 1);

end


%% Nested function to calculate Lorentzian

function l = lorentz(x, lambda, centre, norm, ft)
% Calculate values of a Lorentz function
%
% USE
%   l = lorentz(x, lambda, centre, norm, ft)
%   l = lorentz(x)
%
% IN
%   x       Argument (can be n-dimensional)
%   lambda  Full width at half maximum {1}
%   centre  Position of maximum {0}
%   norm    Normalisation {0}
%           0 : integral = 1
%           1 : maximum  = 1
%   ft      Calculate corresponding exponential instead {0}
%
% OUT
%   l       Function values
%
% (c) Institute for Biomedical Engineering, ETH and University of Zurich, Switzerland

%% Default parameters

default_lambda  = 1;
default_centre  = 0;
default_norm    = 0;
default_ft      = 0;

npar = 1;
if nargin < npar
    error('At least %d input parameter(s) required', npar);
end
npar = npar + 1; if (nargin < npar) || isempty(lambda), lambda = default_lambda; end
npar = npar + 1; if (nargin < npar) || isempty(centre), centre = default_centre; end
npar = npar + 1; if (nargin < npar) || isempty(norm), norm = default_norm; end
npar = npar + 1; if (nargin < npar) || isempty(ft), ft = default_ft; end


%% Calculate Lorentz function

normFac = 2 / pi ./ lambda;

if ~ft
    l = 1 ./ (1 + 4 * ((x-centre) ./ lambda).^2);
    if norm == 0
        l = l .* normFac;
    end
else
    l = exp(- (pi * lambda - 1i * 2*pi * centre) .* abs(x));
    if norm == 1
        l = l ./ normFac;
    end
end

end