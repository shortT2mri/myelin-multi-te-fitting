function p = getPar(parName, par, name)
% Get parameter value from list
%
% (c) Institute for Biomedical Engineering, ETH and University of Zurich, Switzerland

for iPar = 1:length(par)
    if strcmp(parName, name{iPar})
        p = par(iPar);
        return;
    end
end

p = 0;