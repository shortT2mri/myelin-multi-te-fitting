function valComp = computeValComp(parFit, nameFit, nameConst, parConst, teFit, fitType)
% Function to compute the valComp parameter for spiPlot
% 
% IN
%   parFit
%   nameFit
%   nameConst
%   parConst
%   teFit
%   fitType      Choice of fitting procedure, determines how components are separated 
%     
% OUT
%   valComp      Structure containing complex valFit parameters of components
%
% (c) Institute for Biomedical Engineering, ETH and University of Zurich, Switzerland

if isempty(fitType); valComp = []; return; end

%% Set indices of nameFit and nameConst that correspond to each component 
switch fitType
    case 'open' 
        %     nameFit = {'ph' 'df' 'aL' 'aS1' 'aS2' 't2S1' 't2S2' 'dfS1' 'dfS2'};
        %     nameConst = {'typeS1' 'typeS2' 't2L'};
        indConstS1 = [1]; 
        indFitS1 = [1 2 4 6 8];
        indConstS2 = [2];
        indFitS2 = [1 2 5 7 9];
        indConstL = [3];
        indFitL = [1 2 3];
        
        nSCase = '2';
               
    case 'fixed'
%         nameFit     = {'ph' 'df' 'aL' 'aS1' 'aS2'};
%         nameConst   = {'typeS1' 'typeS2' 't2L' 't2S1' 't2S2' 'dfS1' 'dfS2'};
        indConstS1 = [1 4 6];
        indFitS1 = [1 2 4];
        indConstS2 = [2 5 7];
        indFitS2 = [1 2 5];
        indConstL = [3];
        indFitL = [1 2 3];
        
        nSCase = '2';
        
    case 'nS1_open'
        %     nameFit = {'ph' 'df' 'aL' 'aS1' 't2S1' 'dfS1'};
        %     nameConst = {'typeS1' 't2L'};
        indConstS1 = [1];
        indFitS1 = [1 2 4 5 6];
        indConstL = [2];
        indFitL = [1 2 3];

        nSCase = '1';
            
    otherwise
        error('Invalid fitType')
        
end

%% Calculate signal (edit)

switch nSCase
    case '2'
        % nS = 2
        valFitS1 = myelinSignal([parConst(indConstS1) parFit(indFitS1)], teFit, [nameConst(indConstS1) nameFit(indFitS1)], 1);
        valFitS2 = myelinSignal([parConst(indConstS2) parFit(indFitS2)], teFit, [nameConst(indConstS1) nameFit(indFitS1)], 1);
        valFitL  = myelinSignal([parConst(indConstL) parFit(indFitL)], teFit, [nameConst(indConstL) nameFit(indFitL)], 0);
    case '1'
        % nS = 1
        valFitS1 = myelinSignal([parConst(indConstS1) parFit(indFitS1)], teFit, [nameConst(indConstS1) nameFit(indFitS1)], 1);
        valFitL  = myelinSignal([parConst(indConstL) parFit(indFitL)], teFit, [nameConst(indConstL) nameFit(indFitL)], 0);
end

%% Sort s.t. S1 is the component with shortest T2

if nSCase == '2'
    t2S1 = getPar('t2S1', parFit, nameFit);
    t2S2 = getPar('t2S2', parFit, nameFit);
    if t2S2 < t2S1
        origS1 = valFitS1;
        origS2 = valFitS2;
        valFitS1 = origS2;
        valFitS2 = origS1;
    end
end

%% Define valComp components 

switch nSCase
    case '2'
        % nS = 2
        valComp = {valFitS1, valFitS2, valFitL};
        valCompName = {'U', 'S', 'W'};
    case '1'
        % nS = 1
        valComp = {valFitS1, valFitL};
        valCompName = {'U', 'W'};
end

%% Assemble 

valComp = cell2struct(valComp, valCompName, 2);
    

%#ok<*NBRAK2> 
    
