function [val, dfCorr] = performOffResCorr(te, val, dfCorr)
% Perform off-resonance correction
% Uses last two TE values, assuming dominance by water 
%
% IN
%   te       Echo times
%   val      Signal values
%   dfCorr   Correction factor to apply. If function should determine df from fit, do not provide this input
%
% OUT
%   val      Corrected signal values
%   dfCorr   Correction factor
%
% (c) Institute for Biomedical Engineering, ETH and University of Zurich, Switzerland

if nargin < 3 
    nTe = length(te);
    ind = nTe-1:nTe;
    ph = unwrap(angle(val(ind)));
    p = polyfit(te(ind), ph, 1);
    dfCorr = p(1); 
end
val = val .* exp(-1i * dfCorr * te);
