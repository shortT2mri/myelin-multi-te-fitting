function displayFit(pars, parFit, se, data, normA)
% Display values of fitted parameters
% Standard errors appear in brackets
%
% IN
%   pars       Structure containing analysis parameters (see runAnalysis)
%   parFit     Fitted parameter values
%   se         Standard error of parameters
%   normA      Normalisation factor for amplitudes {1}
%
% (c) Institute for Biomedical Engineering, ETH and University of Zurich, Switzerland

if nargin < 5, normA = 1; end

for iPar = 1:length(parFit)
    
    if strcmp(pars.nameFit{iPar}, 'ph')
        fprintf('%5s = %7.3f (%7.3f) rad\n', pars.nameFit{iPar}, parFit(iPar), se(iPar));
    elseif strcmp(pars.nameFit{iPar}, 'df')
        fprintf('%5s = %7.3f (%7.3f) kHz\n', pars.nameFit{iPar}, parFit(iPar), se(iPar));
    elseif strncmp(pars.nameFit{iPar}, 'a', 1)
        fprintf('%5s = %7.3e (%7.3e)\n', pars.nameFit{iPar}, parFit(iPar) * normA, se(iPar) * normA);
    elseif strncmp(pars.nameFit{iPar}, 't2', 2)
        fprintf('%5s = %7.3f (%7.3f) us\n', pars.nameFit{iPar}, parFit(iPar) * 1e3, se(iPar) * 1e3);
    elseif strncmp(pars.nameFit{iPar}, 'dfS', 3)
        fprintf('%5s = %7.3f (%7.3f) ppm\n', pars.nameFit{iPar}, df2cs(parFit(iPar), data.f0), df2cs(se(iPar), data.f0, 1));
    end
end
