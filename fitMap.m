function fitMap(pars, data)
% Calculate parameter maps from multi-TE data series
%
% IN
%   pars     Structure containing analysis parameters (see runAnalysis)
%   data     Structure containing data
%
% (c) Institute for Biomedical Engineering, ETH and University of Zurich, Switzerland


%% Get data and parameters

d = data.d;
te = data.te;
nD = data.nD;
if ~isempty(pars.indTe) % Use select TEs 
    te = te(pars.indTe);
    d = d(:,:,:,pars.indTe); 
end

offResCorr = pars.offResCorr;

%% Adapt slices in mask

mask = data.mask;
% Select slices
if ~isempty(pars.xSlices)
    x = 1:size(mask, 1);
    x(pars.xSlices) = [];
    mask(x, :, :) = 0;
end

%% Get maps by pixel-wise fitting

nFit = sum(mask(:));
mapsList = zeros(nFit, length(pars.nameFit));
resmapList = zeros(nFit, 1);

% Reformat mask into array of voxel positions
idx = find(mask==1);
[a, b, c] = ind2sub(size(mask), idx);
mask = [a,b,c];

% Perform fits
parfor iV = 1:nFit
    cords = mask(iV, :);
    val = squeeze(d(cords(1), cords(2), cords(3), :)).'; 
    
    % Local off-resonance correction
    if offResCorr == 1
        val = performOffResCorr(te, val);
    end
    
    [mapsList(iV, :), resmapList(iV), ~, ~, hasSol(iV)] = myelinFit(val, te, pars.nameFit, pars.nS, pars.parConst, pars.nameConst, pars.nStart, [], pars.sb, 0);  
    
end

% Remove voxel from mask if no solution found
mask(~hasSol,:) = [];
mapsList(~hasSol, :) = [];
resmapList(~hasSol) = [];

nameMaps = [pars.nameFit 'res'];

% Reformat from list of voxels to maps
mapsList = cat(2, mapsList, resmapList);
nMaps = size(mapsList,2);
maps = zeros([nD nMaps]);
for iV = 1:size(mask,1)
    maps(mask(iV,1), mask(iV,2), mask(iV,3), 1:nMaps) = mapsList(iV,:);
end


%% Display

inter = 3; % interpolation factor (see mapVis)
mapVis(maps, nameMaps, pars, inter)


%#ok<*PFBNS> 
%#ok<*PFOUS>