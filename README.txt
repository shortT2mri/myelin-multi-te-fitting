.......................
Myelin multi-TE fitting
.......................

Analysis pipeline accompanying paper "Mapping the myelin bilayer with short-T2 MRI: Methods validation and reference data for healthy human brain" by Baadsvik et al.

Contains Matlab (tested in version 2022a) functions for fitting a myelin signal model to multi-TE data. Example data is also provided.

This code is intended for demonstration purposes only and will not be actively maintained or further developed. 

Authors: Emily Louise Baadsvik, Markus Weiger
(c) Institute for Biomedical Engineering, ETH and University of Zurich, Switzerland


Correspondence email: baadsvik@biomed.ee.ethz.ch

--------------------
Update June 2023
--------------------

Compatibility with paper "Quantitative magnetic resonance mapping of the myelin bilayer reflects pathology in multiple sclerosis brain tissue" by Baadsvik et al.

**************************************************************************************************************
--------------------
USE
--------------------

1. Store all project files locally in the same folder, and open this folder in Matlab's 'Current Folder' interface.
2. Open runAnalysis, change settings as required, then run.

Advanced use:
Certain advanced functionality is included in the Matlab functions, but not used in the analysis example or to generate the
results in the publication. This functionality is made available for the interested user (e.g. if developing a more advanced implementation).

--------------------
CONTENTS
--------------------

* data_D2O / data_H2O
    Data from samples 1Ds / 1Hs 
    Structure ('data') containing:
        d:        Reconstructed images, 4D format (XxYxZxTE; XYZ, spatial, [mm]; T, time, [image index]), interpolated from original 32x32x32 matrix to 64x64x64 matrix
        te:       Echo time of each image [ms]
        nTe:      Number of images (echo times)
        nD:       Matrix size of images
        NSA:      Number of signal averages for each image
        noiseStd: Standard deviation of noise for each image, obtained from dedicated noise measurements
        bw:       Bandwidth of each image [kHz]
        f0:       Scanner frequency [kHz]
        mask:     3D mask of the sample, used to avoid fitting insignificant voxels
        pWM:      Voxel coordinates for WM region (XYZ)
        pGM:      Voxel coordinates for GM region (XYZ)

* runAnalysis
    Calling script for data analysis
    - User options are defined and set in this file
    - Calls either fitComp or fitMap

* fitComp
    Single-run fits of either single voxels or averaged signals from groups of voxels

* fitMap
    Single-voxel fitting to generate parameter maps
    - Parallel-enabled, start parallel pool prior to running if desired

* myelinFit
    Sets up and performs fit to myelin signal model
    - Signal model defined in myelinSignal

* myelinSignal
    Calculates myelin signal values for a given model 
    - Generalised to support one long-T2 component with Lorentzian lineshape (water) and an arbitrary number of additional components (Lorentzian or super-Lorentzian)

* Tools (folder containing files):
    - cs2df / df2cs: Converts between absolute (df, kHz) and relative (cs, ppm) chemical shifts
    - getPar: Extracts parameter value from list
    - superLorentzian: Calculates a discrete super-Lorentzian function (contains nested function to calculate a discrete Lorentzian function)
    - nlparcise: Matlab built-in function, edited to return standard error
    - performOffResCorr: Off-resonance correction from linear fit
    - computeValComp: Computes the signal curve for individual fit components (used by fitPlot)
    - displayFit: Displays fit results in the Command Window (standard errors in brackets)
    - fitPlot: Plots data from multi-TE series, along with fit and fit components if available (used by fitComp)
    - mapVis: Visualises fitted maps (used by fitMap). Only basic visualisation is provided

--------------------
Conventions and naming
--------------------

T2  - Decay constant, either actual T2 (W-component) or T2min (U- and S-components)

CS  - Chemical shift

A   - Component amplitude


Signal model parameters 
    * S1 -> U-component, S2 -> S-component, L -> W-component
    * Note: The two super-Lorentzian components are arbitrarily named S1 and S2 in the code, so unless fit boundaries or component sorting is applied, 
      S1 and S2 can be swapped between what we define in the paper as the U-component (shortest T2) and the S-component
    df: Local off-resonance [kHz]
    dfS1/dfS2: Off-resonance relative to water (chemical shift) [kHz]
    t2S1/t2S2: Decay constant (T2 or T2min) [ms]
    ph: Phase offset [rad]
    aL/aS1/aS2: Component amplitude [arb.]

Coordinate systems:
    * The predefined sample voxel maps (see description for data_D2O/data_H2O above) follow a 3D spatial coordinate 
      system which we define as XYZ. These coordinates correspond to the array structure in the example data 
    * The Matlab coordinates when displaying image slices (specifically using the mapVis function) do not correspond
      to the original data format
        X in the Matlab figure -> Z in data coordinates
        Y in the Matlab figure -> Y in data coordinates
        Z (through-slice in Matlab figure) -> X in data coordinates
